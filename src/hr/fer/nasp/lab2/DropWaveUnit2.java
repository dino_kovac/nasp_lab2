
package hr.fer.nasp.lab2;

public class DropWaveUnit2 extends DropWaveUnit {

    @Override
    public Unit cross(Unit mate) {
        DropWaveUnit child = new DropWaveUnit();

        if (x1 < ((DropWaveUnit)mate).x1)
            child.x1 = Helper.random(x1, ((DropWaveUnit)mate).x1);
        else
            child.x1 = Helper.random(((DropWaveUnit)mate).x1, x1);

        if (x2 < ((DropWaveUnit)mate).x2)
            child.x2 = Helper.random(x2, ((DropWaveUnit)mate).x2);
        else
            child.x2 = Helper.random(((DropWaveUnit)mate).x2, x2);

        return child;
    }

}
