
package hr.fer.nasp.lab2;

abstract public class Unit implements Comparable<Unit> {

    /**
     * a metric of the quality of the unit, needs to be positive
     */
    protected Double fitness;

    public double getFitness() {
        return fitness;
    }

    abstract public void print();

    abstract public Unit cross(Unit mate);

    abstract public void mutate();

    /**
     * Randomizes the chromosomes of the unit and updates {@link #fitness}
     */
    abstract public void randomize();

    abstract public void updateFitness();

    @Override
    public int compareTo(Unit o) {
        return this.fitness.compareTo(o.fitness);
    }

}
