
package hr.fer.nasp.lab2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GeneticAlgorithm {

    private List<Unit> currentGeneration;
    private List<Unit> nextGeneration;

    private UnitFactory<? extends Unit> unitFactory;

    private ParentSelection selector;

    private boolean elitismEnabled = true;
    private int populationSize = 50;
    private float mutationChance = 0.1f;

    public int getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(int populationSize) {
        assert (populationSize > 1);

        this.populationSize = populationSize;
    }

    public boolean isElitismEnabled() {
        return elitismEnabled;
    }

    public void setElitismEnabled(boolean elitismEnabled) {
        this.elitismEnabled = elitismEnabled;
    }

    public ParentSelection getSelector() {
        return selector;
    }

    public void setSelector(ParentSelection selector) {
        this.selector = selector;
    }

    public float getMutationChance() {
        return mutationChance;
    }

    public void setMutationChance(float mutationChance) {
        this.mutationChance = mutationChance;
    }

    public GeneticAlgorithm(UnitFactory<? extends Unit> unitFactory, ParentSelection selector) {
        this.unitFactory = unitFactory;
        this.selector = selector;
    }

    /**
     * Runs the algorithm for a number of generations
     * 
     * @param generations how many generations to run the algorithm for
     */
    public Unit run(int generations) {

        initializeFirstGeneration();

        for (int i = 0; i < generations; i++) {
            nextGeneration();
            getFittest().print();
        }

        return getFittest();
    }

    /**
     * Generates a random generation of size {@link #populationSize}
     */
    private void initializeFirstGeneration() {
        currentGeneration = new ArrayList<>(getPopulationSize());
        for (int i = 0; i < getPopulationSize(); i++) {
            Unit randomUnit = unitFactory.create();
            randomUnit.randomize();
            randomUnit.updateFitness();
            currentGeneration.add(randomUnit);
        }

        getFittest().print();
    }

    /**
     * Produces a new generation from the current generation using crossing and
     * mutations
     */
    private void nextGeneration() {

        nextGeneration = new ArrayList<>(getPopulationSize());

        for (int i = 0; i < getPopulationSize(); i++) {
            List<Unit> parents = selector.select(currentGeneration);
            assert (parents.size() > 1);

            Unit child = parents.get(0).cross(parents.get(1));

            while (Helper.random() < mutationChance)
                child.mutate();

            child.updateFitness();

            nextGeneration.add(child);
        }

        Unit fittest = getFittest();

        currentGeneration = nextGeneration;
        nextGeneration = null;

        if (elitismEnabled) {
            currentGeneration.remove(getLeastFit());
            currentGeneration.add(fittest);
        }

    }

    /**
     * Returns the fittest unit in the current generation
     * 
     * @return the fittest unit
     */
    private Unit getFittest() {
        Unit fittest = currentGeneration.get(0);
        for (Unit unit : currentGeneration) {
            if (unit.getFitness() > fittest.getFitness())
                fittest = unit;
        }

        return fittest;
    }

    /**
     * Returns the least fit unit in the current generation
     * 
     * @return the least fit unit
     */
    private Unit getLeastFit() {
        Unit leastFit = currentGeneration.get(0);
        for (Unit unit : currentGeneration) {
            if (unit.getFitness() < leastFit.getFitness())
                leastFit = unit;
        }

        return leastFit;
    }

    public void printGeneration() {
        Collections.sort(currentGeneration);
        for (Unit unit : currentGeneration) {
            unit.print();
        }
    }

}
