
package hr.fer.nasp.lab2;

public class Helper {

    public static double random(double x1, double x12) {
        return x1 + (int)(Math.random() * ((x12 - x1) + 1));
    }

    public static double random(float max) {
        return random(0, max);
    }

    public static double random() {
        return random(0, 1);
    }

}
