
package hr.fer.nasp.lab2;

public class UnitFactory<T extends Unit> {

    private Class<T> unitClass;

    @SuppressWarnings("unused")
    private UnitFactory() {

    }

    public UnitFactory(Class<T> unitClass) {
        this.unitClass = unitClass;
    }

    public T create() {
        try {
            return unitClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
