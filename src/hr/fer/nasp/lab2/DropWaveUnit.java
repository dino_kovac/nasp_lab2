
package hr.fer.nasp.lab2;

public class DropWaveUnit extends Unit {

    protected final float MIN = -5.12f;
    protected final float MAX = 5.12f;

    protected double x1;
    protected double x2;

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getX2() {
        return x2;
    }

    public void setX2(double x2) {
        this.x2 = x2;
    }

    @Override
    public String toString() {
        return String.format("(%.3f, %.3f) => %.3f", x1, x2, fitness);
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    /**
     * Cross by taking the average of the parameters of the parents
     */
    @Override
    public Unit cross(Unit mate) {
        DropWaveUnit child = new DropWaveUnit();

        child.x1 = (x1 + ((DropWaveUnit)mate).x1) / 2;
        child.x2 = (x2 + ((DropWaveUnit)mate).x2) / 2;

        return child;
    }

    @Override
    public void mutate() {
        if (Math.random() < 0.5f)
            x1 += Helper.random(-0.2f, 0.2f);
        else
            x2 += Helper.random(-0.2f, 0.2f);

        normalize();
    }

    protected void normalize() {
        if (x1 < MIN)
            x1 = MIN;
        if (x1 > MAX)
            x1 = MAX;
        if (x2 < MIN)
            x2 = MIN;
        if (x2 > MAX)
            x2 = MAX;
    }

    @Override
    public void randomize() {
        x1 = Helper.random(MIN, MAX);
        x2 = Helper.random(MIN, MAX);
    }

    @Override
    public void updateFitness() {
        fitness = -calculateDropWave();
    }

    protected double calculateDropWave() {
        double sumOfSquares = Math.sqrt(x1 * x1 + x2 * x2);
        double top = 1 + Math.cos(12 * sumOfSquares);
        double bottom = 0.5 * sumOfSquares + 2;

        return -(top / bottom);
    }
}
