
package hr.fer.nasp.lab2;

import java.util.ArrayList;
import java.util.List;

public class RouletteWheelParentSelection implements ParentSelection {

    @Override
    public List<Unit> select(List<Unit> units) {

        List<Unit> parents = new ArrayList<>(2);

        double fitnessSum = 0;
        for (Unit unit : units) {
            fitnessSum += unit.getFitness();
        }

        double chance = Math.random() * fitnessSum;
        parents.add(selectParent(units, chance));
        chance = (float)(Math.random() * fitnessSum);
        parents.add(selectParent(units, chance));

        return parents;
    }

    private Unit selectParent(List<Unit> units, double chance) {
        float fitnessSum = 0;
        for (Unit unit : units) {
            if (chance >= fitnessSum && chance < (fitnessSum + unit.getFitness()))
                return unit;
            fitnessSum += unit.getFitness();
        }

        return units.get(units.size() - 1);
    }
}
