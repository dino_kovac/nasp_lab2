
package hr.fer.nasp.lab2;

public class Test {

    public static void main(String[] args) {

        int generations = 100;

        if (args.length > 0) {
            try {
                generations = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        UnitFactory<DropWaveUnit> unitFactory = new UnitFactory<>(DropWaveUnit.class);
        GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm(unitFactory, new RouletteWheelParentSelection());

        Unit u = geneticAlgorithm.run(generations);
        System.out.println("##########\nSolution: " + u.toString() + "\n");

        geneticAlgorithm = new GeneticAlgorithm(unitFactory, new RouletteWheelParentSelection());
        geneticAlgorithm.setElitismEnabled(false);

        Unit u2 = geneticAlgorithm.run(generations);
        System.out.println("##########\nSolution: " + u2.toString() + "\n");

        UnitFactory<DropWaveUnit2> unitFactory2 = new UnitFactory<>(DropWaveUnit2.class);
        geneticAlgorithm = new GeneticAlgorithm(unitFactory2, new RouletteWheelParentSelection());

        Unit u3 = geneticAlgorithm.run(generations);
        System.out.println("##########\nSolution: " + u3.toString() + "\n");

        System.out.println("##########\nSolution1: " + u.toString());
        System.out.println("Solution2: " + u2.toString());
        System.out.println("Solution3: " + u3.toString() + "\n");
    }
}
