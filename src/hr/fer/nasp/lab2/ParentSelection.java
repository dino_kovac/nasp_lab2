
package hr.fer.nasp.lab2;

import java.util.List;

public interface ParentSelection {

    public List<Unit> select(List<Unit> units);

}
